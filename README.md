 
## What did I do?

- The project is written in a modular way.
- Used: Traits, Transactions, Pessimistic Locking, Exception handler, Feature test, Seeder, Api documentation
- Postman Collection uploaded in project root 
 
### Documentation
- **[Project Api Document](https://127.0.0.1:8000/docs)**
  
