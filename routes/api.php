<?php

use App\Http\Controllers\Api\AccountController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::controller(AccountController::class)->prefix('account')->name('account.')->group(function () {
//    Route::post('/create', 'create')->name('create');
//    Route::post('/transfer', 'transfer')->name('transfer');
//    Route::get('/retrieve-balance/{account}', 'getBalance')->name('retrieve-balance');
//    Route::get('/transfer-history', 'getTransferHistory')->name('transfer-history');
//});

