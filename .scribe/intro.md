# Introduction



This documentation aims to provide all the information you need to work with our API.

<aside>While modern banks have evolved to serve a plethora of functions, at their core, banks must
provide certain basic features. Today, your task is to build the basic HTTP API for one of
those banks! Imagine you are designing a backend API for bank employees. It could
ultimately be consumed by multiple frontends (web, iOS, Android etc).</aside>

> Base URL

```yaml
127.0.0.1:8000
```