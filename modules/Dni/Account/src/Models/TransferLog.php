<?php

namespace Dni\Account\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferLog extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'source_id',
        'destination_id',
        'amount',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];


    public function source(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Account::class , "source_id");
    }

    public function destination(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Account::class , "destination_id");
    }

}
