<?php

namespace Dni\Account\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'balance',


    ];



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
