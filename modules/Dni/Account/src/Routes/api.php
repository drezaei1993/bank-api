<?php
use Illuminate\Support\Facades\Route;
Route::name("account.")->prefix("account")->namespace("Dni\Account\Http\Controllers\Api")->group(function ($router) {
    $router->post('/create', "AccountController@create")->name("create");
    $router->post('/transfer', 'AccountController@transfer')->name('transfer');
    $router->get('/retrieve-balance/{account}', 'AccountController@getBalance')->name('retrieve-balance');
    $router->get('/transfer-history', 'AccountController@getTransferHistory')->name('transfer-history');
});



