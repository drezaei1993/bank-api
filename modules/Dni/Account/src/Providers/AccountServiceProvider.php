<?php

namespace Dni\Account\Providers;


use Database\Seeders\DatabaseSeeder;
use Dni\Account\Database\Seeds\UserTableSeeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AccountServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->loadMigrationsFrom(__DIR__ . "/../Database/Migrations");
        DatabaseSeeder::$seeders[] = UserTableSeeder::class;

    }

    public function boot()
    {
        Route::middleware('api')->prefix('api')->group(function () {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');
        });
    }
}
