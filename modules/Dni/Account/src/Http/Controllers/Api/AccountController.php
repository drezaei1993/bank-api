<?php

namespace Dni\Account\Http\Controllers\Api;

use App\Http\Controllers\BaseController as BaseController;
use Dni\Account\Http\Requests\CreateRequest;
use Dni\Account\Http\Requests\TransferLogRequest;
use Dni\Account\Http\Requests\TransferRequest;
use Dni\Account\Http\Resources\AccountResource;
use Dni\Account\Http\Resources\TransferLogCollection;
use Dni\Account\Models\Account;
use Dni\Account\Models\TransferLog;
use Dni\Account\Traits\AccountTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

/**
 * @group Account
 *
 * API endpoints for managing account
 */
class AccountController extends BaseController
{
    use AccountTrait;

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     */
    public function create(CreateRequest $request): JsonResponse
    {
        DB::transaction(function () use ($request) {
            $user = $this->createOrGetUser($request);
            $this->createAccount($user, $request);
        });
        return $this->sendResponse('Account generated!');
    }


    /**
     * @param TransferRequest $request
     * @return JsonResponse
     */
    public function transfer(TransferRequest $request): JsonResponse
    {
        DB::transaction(function () use ($request) {
            $sourceAccount = Account::lockForUpdate()->find($request->source_account);
            $this->checkSourceAccountBalance($request, $sourceAccount);
            $this->transferBalance($sourceAccount, $request);
            $this->generateTransferLog($request);
        });
        return $this->sendResponse('The transfer was successful');
    }

    /**
     * @param Account $account
     * @return AccountResource
     */
    public function getBalance(Account $account): AccountResource
    {
        return new AccountResource($account);
    }


    /**
     * @param TransferLogRequest $transferLog
     * @return TransferLogCollection
     */
    public function getTransferHistory(TransferLogRequest $transferLog): TransferLogCollection
    {
        return new TransferLogCollection(TransferLog::where('source_id', $transferLog->account_id)->orWhere('destination_id', $transferLog->account_id)->get());
    }


}
