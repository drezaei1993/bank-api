<?php

namespace Dni\Account\Database\Seeds;


use Dni\Account\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (User::$defaultUsers as $user) {
            User::firstOrCreate(  ['name' => $user['name']   ]);
        }
    }
}
