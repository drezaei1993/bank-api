<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_logs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('source_id');
            $table->foreign('source_id')->references('id')->on('accounts');

            $table->unsignedBigInteger('destination_id');
            $table->foreign('destination_id')->references('id')->on('accounts');

            $table->unsignedDecimal('amount')->default(0);

            $table->timestamp("created_at");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_logs');
    }
};
