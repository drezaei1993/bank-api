<?php

namespace Dni\Account\Traits;
use Dni\Account\Http\Requests\CreateRequest;
use Dni\Account\Http\Requests\TransferRequest;
use Dni\Account\Models\Account;
use Dni\Account\Models\TransferLog;
use Dni\Account\Models\User;
use Illuminate\Validation\ValidationException;

trait AccountTrait
{

    /**
     * @param CreateRequest $request
     * @return mixed
     */
    private function createOrGetUser(CreateRequest $request): mixed
    {
        return User::firstOrCreate(
            ['name' => $request->name]
        );
    }


    /**
     * @param mixed $user
     * @param CreateRequest $request
     * @return void
     */
    private function createAccount(mixed $user, CreateRequest $request): void
    {
        $user->accounts()->create([
            'balance' => $request->balance
        ]);
    }


    /**
     * @param TransferRequest $request
     * @param mixed $sourceAccount
     * @return void
     * @throws ValidationException
     */
    private function checkSourceAccountBalance(TransferRequest $request, mixed $sourceAccount): void
    {
        if ($request->amount > $sourceAccount->balance) {
            throw ValidationException::withMessages([
                'balance' => 'The balance of the source account is insufficient'
            ]);
        }
    }


    /**
     * @param mixed $sourceAccount
     * @param TransferRequest $request
     * @return void
     */
    private function transferBalance(mixed $sourceAccount, TransferRequest $request): void
    {
        $sourceAccount->decrement('balance', $request->amount);
        Account::lockForUpdate()->find($request->destination_account)->increment('balance', $request->amount);
    }

    /**
     * @param TransferRequest $request
     * @return void
     */
    private function generateTransferLog(TransferRequest $request): void
    {
        TransferLog::create([
            'source_id' => $request->source_account,
            'destination_id' => $request->destination_account,
            'amount' => $request->amount
        ]);
    }

}
