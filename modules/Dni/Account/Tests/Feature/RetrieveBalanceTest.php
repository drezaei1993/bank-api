<?php

namespace Dni\Account\Tests\Feature;
use Dni\Account\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class RetrieveBalanceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @throws \Throwable
     */
    public function test_user_can_retrieve_balance()
    {
        $balance = 1000;
        $this->createUserAndAccount("jafar", $balance);
        $account = User::whereName("jafar")->first()->accounts()->first();
        $response = $this->get(route('account.retrieve-balance', $account->id));
        $this->assertEquals($response->decodeResponseJson()['data']['balance'], $balance);
    }


    /**
     * @param string $name
     * @param $balance
     * @return TestResponse
     */
    public function createUserAndAccount(string $name, $balance): TestResponse
    {
        return $this->post(route('account.create'), [
            'name' => $name,
            'balance' => $balance,
        ]);
    }
}
