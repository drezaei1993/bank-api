<?php

namespace Dni\Account\Tests\Feature;

use Dni\Account\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_create_account(): void
    {
        $response =  $this->createUserAndAccount("jafar",1001);
        $response->assertStatus(200);
    }

    public function test_user_cant_create_account_for_balance_less_than_1000(): void
    {
        $response =  $this->createUserAndAccount("jafar",999);
        $response->assertStatus(422);
    }

    public function test_each_user_can_have_multiple_accounts(): void
    {
        $name = 'ali';
        $this->createUserAndAccount($name,2000);
        $this->createUserAndAccount($name,3000);
        $this->assertEquals(2,User::whereName($name)->withCount('accounts')->first()->accounts_count) ;
    }


    /**
     * @param string $name
     * @param $balance
     * @return TestResponse
     */
    public function createUserAndAccount(string $name , $balance): TestResponse
    {
     return   $this->post(route('account.create'), [
            'name' => $name,
            'balance' => $balance,
        ]);
    }
}
