<?php

namespace Dni\Account\Tests\Feature;

use Dni\Account\Models\TransferLog;
use Dni\Account\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class TransferTest extends TestCase
{
    use RefreshDatabase;


    public function test_user_can_transfer_balance()
    {
        $response = $this->generateTransfer(3000, 3000, 3000);
        $response->assertStatus(200);
        $this->assertEquals(0, User::whereName("ali")->first()->accounts()->first()->balance);
        $this->assertEquals(6000, User::whereName("jafar")->first()->accounts()->first()->balance);
    }


    public function test_generate_transfer_log_after_success_transfer()
    {
        $this->generateTransfer(3000, 3000, 3000);
        $this->assertEquals(1, TransferLog::count());

    }

    public function test_source_cant_transfer_when_amount_is_bigger_than_balance()
    {
        $response = $this->generateTransfer(2000, 3000, 3000);
        $response->assertStatus(422);

    }


    public function createUserAndAccount(string $name, $balance): TestResponse
    {
        return $this->post(route('account.create'), [
            'name' => $name,
            'balance' => $balance,
        ]);
    }

    /**
     * @param $sourceBalance
     * @param $destinationBalance
     * @param $transferAmount
     * @return TestResponse
     */
    public function generateTransfer($sourceBalance, $destinationBalance, $transferAmount): TestResponse
    {
        $this->createUserAndAccount("ali", $sourceBalance);
        $this->createUserAndAccount("jafar", $destinationBalance);
        $sourceAccount = User::whereName("ali")->first()->accounts()->first();
        $destinationAccount = User::whereName("jafar")->first()->accounts()->first();

        return $this->post(route('account.transfer'), [
            'source_account' => $sourceAccount->id,
            'destination_account' => $destinationAccount->id,
            'amount' => $transferAmount,
        ]);
    }
}
