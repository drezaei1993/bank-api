<?php

namespace Dni\Account\Tests\Feature;
use Dni\Account\Models\TransferLog;
use Dni\Account\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class TransferHistoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_generate_transfer_log_after_success_transfer()
    {
        $this->generateTransfer(3000, 3000, 3000);
        $this->assertEquals(1, TransferLog::count());

    }

    public function test_get_transfer_logs()
    {
        $this->generateTransfer(3000, 3000, 3000);
        $sourceAccount = User::whereName("ali")->first()->accounts()->first();
         $this->post(route('account.transfer-history'), [
            'account_id' => $sourceAccount->id,
        ]);
        $this->assertEquals(1, TransferLog::count());

    }


    /**
     * @param $sourceBalance
     * @param $destinationBalance
     * @param $transferAmount
     * @return TestResponse
     */
    public function generateTransfer($sourceBalance, $destinationBalance, $transferAmount): TestResponse
    {
        $this->createUserAndAccount("ali", $sourceBalance);
        $this->createUserAndAccount("jafar", $destinationBalance);
        $sourceAccount = User::whereName("ali")->first()->accounts()->first();
        $destinationAccount = User::whereName("jafar")->first()->accounts()->first();

        return $this->post(route('account.transfer'), [
            'source_account' => $sourceAccount->id,
            'destination_account' => $destinationAccount->id,
            'amount' => $transferAmount,
        ]);
    }


    public function createUserAndAccount(string $name, $balance): TestResponse
    {
        return $this->post(route('account.create'), [
            'name' => $name,
            'balance' => $balance,
        ]);
    }
}
